var path = require("path");
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: "./app/scripts/main.js",
  output: {
    path: path.resolve(__dirname, 'dest'),
    // path: __dirname + '/dest',
    // publicPath: "/dest/",
    filename: "app.js"
  },
  cache: true,
  debug: true,
  devtool: 'source-map',
  module: {
    loaders: [
      { test: /\.css$/, loader: "style!css" },
      { test: /\.scss$/, loader: "style!css!sass" },
      { test: /\.styl$/, loader: "style!css!stylus-loader?paths=node_modules/bootstrap-stylus/stylus/" },
      { test: /\.pug/, loader: "pug" },
      { test: /\.png/, loader: "file!img" },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel', // 'babel-loader' is also a legal name to reference
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './app/index.pug'
    })
  ]
};