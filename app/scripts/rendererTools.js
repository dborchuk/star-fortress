import PIXI from 'pixi.js';

module.exports = {
  // create a texture from an image path
  cteateTexture: path => PIXI.Texture.fromImage(path),
  // returns an empty texture
  getEmptyTexture: () => PIXI.Texture.EMPTY,
  // create a new Sprite using the texture
  createSprite: texture => new PIXI.Sprite(texture),
  // create PIXI container
  createContainer: () => new PIXI.Container(),
  // create PIXI text element
  createText: (text, styles) => new PIXI.Text(text, styles),
  // Clear PIXI container
  clearContainer: (container) => {
    const length = container.children.length;

    if (length) {
      container.removeChildren(0, length - 1);
    }
  }
};