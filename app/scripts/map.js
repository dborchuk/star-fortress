// var log = require(['./log.js']);
import log from './log.js';
// var MetalBlock = require(['./map-objects/metal-block.js']);
import MetalBlock from './map-objects/metal-block.js';
import Frame from './map-objects/frame.js';
// var Player = require(['./map-objects/player.js']);
import Player from './map-objects/player.js';
// var Ladder = require(['./map-objects/ladder.js']);
import Ladder from './map-objects/ladder.js';
// var store = require(['./store.js']);
import store from './store.js';
import _ from 'lodash';

var constructors, constructorsByTypes, create, generate, getMapKey, getPassMatrix;
// constructors = [MetalBlock, Ladder, Player];
// constructorsByTypes = {};
// constructors.map(function(item) {
//   return constructorsByTypes[item.name] = item;
// });
// generate = function() {
//   var i, tiles;
//   tiles = [];
//   i = 0;
//   while (i < 5) {
//     tiles.push(new MetalBlock({
//       isBackground: true,
//       x: i,
//       y: 0,
//       z: 0
//     }));
//     tiles.push(new MetalBlock({
//       x: i,
//       y: 0,
//       z: 1
//     }));
//     i++;
//   }
//   return create({
//     _tiles: tiles
//   });
// };
// getPassMatrix = function(map, y, z) {
//   var blockerCoords, coords, coordsX, coordsY, floorTiles, i, j, lengthX, lengthY, matrix, maxX, maxY, minX, minY, offsetX, offsetY, row, supportCoords, supportTiles, tiles, tilesKeys, zLevel;
//   tilesKeys = Object.keys(map._tiles);
//   tiles = tilesKeys.map(function(key) {
//     return map._tiles[key];
//   }).filter(function(tile) {
//     return !!tile;
//   });
//   zLevel = 1;
//   floorTiles = tiles.filter(function(tile) {
//     return !tile.building;
//   }).filter(function(tile) {
//     return tile.z === zLevel;
//   }).filter(function(tile) {
//     return _.contains(tile.tags, 'blocker');
//   }).filter(function(tile) {
//     var topTile;
//     topTile = map.get(tile.x, tile.y - 1, tile.z);
//     return !topTile || !_.contains(topTile.tags, 'blocker');
//   });
//   supportTiles = tiles.filter(function(tile) {
//     return !tile.building;
//   }).filter(function(tile) {
//     return tile.z === zLevel;
//   }).filter(function(tile) {
//     return _.contains(tile.tags, 'support') && !_.contains(tile.tags, 'blocker');
//   });
//   blockerCoords = floorTiles.map(function(tile) {
//     return {
//       x: tile.x,
//       y: tile.y - 1
//     };
//   });
//   supportCoords = [];
//   _.each(supportTiles, function(tile) {
//     var topTile;
//     supportCoords.push({
//       x: tile.x,
//       y: tile.y
//     });
//     topTile = map.get(tile.x, tile.y - 1, tile.z);
//     if (!topTile || !_.contains(topTile.tags, 'blocker')) {
//       return supportCoords.push({
//         x: tile.x,
//         y: tile.y - 1
//       });
//     }
//   });
//   coords = blockerCoords.concat(supportCoords);
//   coordsX = coords.map(function(coord) {
//     return coord.x;
//   });
//   coordsY = coords.map(function(coord) {
//     return coord.y;
//   });
//   maxX = _.max(coordsX);
//   maxY = _.max(coordsY);
//   minX = _.min(coordsX);
//   minY = _.min(coordsY);
//   offsetX = minX - 1;
//   offsetY = minY - 1;
//   lengthX = maxX - offsetX + 2;
//   lengthY = maxY - offsetY + 2;
//   matrix = [];
//   i = 0;
//   while (i < lengthX) {
//     row = [];
//     j = 0;
//     while (j < lengthY) {
//       row.push(1);
//       j++;
//     }
//     matrix.push(row);
//     i++;
//   }
//   _.each(coords, function(coord) {
//     return matrix[coord.x - offsetX][coord.y - offsetY] = 0;
//   });
//   matrix.offsetX = offsetX;
//   matrix.offsetY = offsetY;
//   matrix.lengthX = lengthX;
//   matrix.lengthY = lengthY;
//   return matrix;
// };
function create (map) {
  let newMap = {};
  let constructors = [Frame, MetalBlock, Ladder, Player];
  let constructorsByTypes = {};
  constructors.map(function(item) {
    return constructorsByTypes[item.name] = item;
  });

  for (let key in map) {
    if ( map.hasOwnProperty(key) ) {
      newMap[key] = map[key].map((item) => {
        return new constructorsByTypes[item.type](item);
      });
      // newTiles.push()
    }
  }
  return newMap;

  // var newTiles;
  // newTiles = [];
  // map.items.forEach((tile) => {
  //   if (tile) {
  //     return newTiles.push(new constructorsByTypes[tile.type](tile));
  //   }
  // });
  // return {
  //   getAll: function(x, y, z) {
  //     var point, result;
  //     if (_.isObject(x)) {
  //       point = x;
  //       x = point.x;
  //       y = point.y;
  //       z = point.z;
  //     }
  //     result = _.filter(this._tiles, function(tile) {
  //       return tile && tile.x === x && tile.y === y && tile.z === z;
  //     });
  //     return result;
  //   },
  //   get: function(x, y, z) {
  //     var point, result;
  //     if (_.isObject(x)) {
  //       point = x;
  //       x = point.x;
  //       y = point.y;
  //       z = point.z;
  //     }
  //     result = _.find(this._tiles, function(tile) {
  //       return tile && tile.x === x && tile.y === y && tile.z === z;
  //     });
  //     return result;
  //   },
  //   add: function(tile) {
  //     return this._tiles.push(tile);
  //   },
  //   remove: function(x, y, z) {
  //     var index, point;
  //     if (_.isObject(x)) {
  //       point = x;
  //       x = point.x;
  //       y = point.y;
  //       z = point.z;
  //     }
  //     index = -1;
  //     this._tiles.some(function(tile, _index) {
  //       if (tile && tile.x === x && tile.y === y && tile.z === z) {
  //         index = _index;
  //         return true;
  //       }
  //       return false;
  //     });
  //     if (index >= 0) {
  //       this._tiles.splice(index, 1);
  //       return true;
  //     }
  //     return false;
  //   },
  //   getPassMatrix: getPassMatrix,
  //   _tiles: newTiles
  // };
};
// getMapKey = function(point) {
//   return point.x + ',' + point.y + ',' + point.z;
// };

class Map {
  constructor(config) {
    config = config || {};

    this.items = config.items || {};
  }
  remove(id, point) {
    let key = getKey(point);

    removeItemFromTile(this.items[key], id);
  }
  add(item, point) {
    let key = getKey(point);
    addItemToTile(this.items, key, item)
  }
  get(point) {
    let key = getKey(point);

    return this.items[key] || [];
  }
  save() {
    return store.set('StarFortress.map', this.items);
  }
  static load() {
    var map, tiles;
    map = null;
    tiles = store.get('StarFortress.map');
    if (tiles) {
      map = create(tiles);
    }
    return new Map({
      items: map
    });
  }
}

module.exports = Map;
// module.exports = {
//   generate: function() {
//     return generate();
//   },
//   save: function(map) {
//     return store.set('StarFortress.map', map);
//   },
//   load: function() {
//     var map, tiles;
//     map = null;
//     tiles = store.get('StarFortress.map');
//     if (tiles) {
//       map = create(tiles);
//     }
//     return map;
//   },
//   getMapKey: getMapKey
// };

function getKey(point) {
  return point.x + ',' + point.y;
}

function removeItemFromTile(tile, id) {
  let index = tile.findIndex( item => item.id === id );
  index && tile.splice(index, 1);
}

function addItemToTile(tiles, key, item) {
  if (!tiles[key]) {
    tiles[key] = [];
  }
  tiles[key].push(item);
}