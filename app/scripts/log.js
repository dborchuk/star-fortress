// var config = require(['./config.js']);
import config from './config.js';

module.exports = function(msg) {
  if (config.debug) {
    return console.log(msg);
  }
};
