define(
  [
    'scripts/log'
    'scripts/map-objects/metal-block'
    'scripts/map-objects/player'
    'scripts/map-objects/ladder'
    'scripts/store'
  ],
  (log, MetalBlock, Player, Ladder, store) ->
    log('Module: Map')

    constructors = [
      MetalBlock
      Ladder
      Player
    ]
    constructorsByTypes = {}
    constructors.map(
      (item) -> constructorsByTypes[item.type] = item
    )

    generate = () ->
      tiles = []
      i = 0
      while i < 5
        tiles.push(
          MetalBlock({
            isBackground: true
            x: i
            y: 0
            z: 0
          })
        )
        tiles.push(
          MetalBlock({
            x: i
            y: 0
            z: 1
          })
        )
        i++

      return create({_tiles: tiles})

    getPassMatrix = (map, y, z) ->
      tilesKeys = Object.keys(map._tiles)
      tiles = tilesKeys.map( (key) -> map._tiles[key] )
        .filter( (tile) -> !!tile )

      zLevel = 1

      floorTiles = tiles.filter( (tile) -> !tile.building )
        .filter( (tile) -> tile.z == zLevel )
        .filter( (tile) -> _.contains(tile.tags, 'blocker')
        )
        .filter( (tile) ->
          topTile = map.get(tile.x, tile.y - 1, tile.z)
          !topTile or !_.contains(topTile.tags, 'blocker')
        )

      supportTiles = tiles.filter( (tile) -> !tile.building )
        .filter( (tile) -> tile.z == zLevel )
        .filter( (tile) ->
          _.contains(tile.tags, 'support') and !_.contains(tile.tags, 'blocker')
        )

      blockerCoords = floorTiles.map( (tile) -> {x: tile.x, y: tile.y - 1} )
      supportCoords = []
      _.each(
        supportTiles,
        (tile) -> 
          supportCoords.push {x: tile.x, y: tile.y}
          topTile = map.get(tile.x, tile.y - 1, tile.z)
          if !topTile or !_.contains(topTile.tags, 'blocker')
            supportCoords.push {x: tile.x, y: tile.y - 1}
        )
      coords = blockerCoords.concat(supportCoords)

      coordsX = coords.map( (coord) -> coord.x )
      coordsY = coords.map( (coord) -> coord.y )
      maxX = _.max(coordsX)
      maxY = _.max(coordsY)
      minX = _.min(coordsX)
      minY = _.min(coordsY)

      offsetX = minX - 1
      offsetY = minY - 1

      lengthX = maxX - offsetX + 2
      lengthY = maxY - offsetY + 2

      # Creating matrix
      matrix = []
      i = 0
      while i < lengthX
        row = []
        j = 0
        while j < lengthY
          row.push(1)
          j++
        matrix.push(row)
        i++

      _.each(coords, (coord) -> matrix[coord.x - offsetX][coord.y - offsetY] = 0)

      matrix.offsetX = offsetX
      matrix.offsetY = offsetY

      matrix.lengthX = lengthX
      matrix.lengthY = lengthY

      return matrix

    create = (map) ->
      newTiles = []
      map._tiles.forEach(
        (tile) ->
          if tile
            newTiles.push( constructorsByTypes[tile.type](tile) )
      )
      # for key, tile of map._tiles
      #   if tile
      #     newTiles[key] = constructorsByTypes[tile.type](tile)
      return {
        getAll: (x, y, z) ->
          if _.isObject(x)
            point = x
            x = point.x
            y = point.y
            z = point.z
          result = _.filter(@_tiles, (tile) -> tile and tile.x == x and tile.y == y and tile.z == z )

          return result

        get: (x, y, z) ->
          if _.isObject(x)
            point = x
            x = point.x
            y = point.y
            z = point.z
          result = _.find(@_tiles, (tile) -> tile and tile.x == x and tile.y == y and tile.z == z )

          return result

          # return @_tiles[x + ',' + y + ',' + z]
        add: (tile) ->
          # remove(x, y, z)
          # if _.isObject(x)
          #   point = x
          #   tile = y
          #   x = point.x
          #   y = point.y
          #   z = point.z
          # if tile
          #   tile.x = x
          #   tile.y = y
          #   tile.z = z
            
          # TODO:
          # if !indexof(tile)
          return @_tiles.push(tile)
        remove: (x, y, z) ->
          if _.isObject(x)
            point = x
            x = point.x
            y = point.y
            z = point.z
          index = -1
          @_tiles.some(
            (tile, _index) ->
              if tile and tile.x == x and tile.y == y and tile.z == z 
                index = _index
                return true
              return false
          )
          if index >= 0
            @_tiles.splice(index, 1)
            return true

          return false
        getPassMatrix: getPassMatrix
        _tiles: newTiles
      }

    getMapKey = (point) ->
      return point.x + ',' + point.y + ',' + point.z

    return {
      generate: () ->
        generate()
      save: (map) ->
        store.set('StarFortress.map', map)
      load: () ->
        map = null
        tiles = store.get('StarFortress.map')
        if tiles
          map = create(tiles)
        return map
      getMapKey: getMapKey
    }
)