define(
  [],
  () ->
    tasks = []

    done = () ->
      index = _.indexOf(tasks, @)
      tasks.splice(index, 1)
      @.done = null

    return {
      get: ->
        freeTasks = tasks.filter (task) -> !task.assignee
        return freeTasks[0]
      getAll: ->
        freeTasks = tasks.filter (task) -> !task.assignee
        return freeTasks

      add: (task) ->
        task.done = _.bind(done, task)
        tasks.push task
    }
)