define(
  [
    'scripts/map'
    'scripts/log'
    'scripts/map-objects/metal-block'
    'scripts/map-objects/player'
    'scripts/map-objects/gun'
    'scripts/map-objects/drone'
    'scripts/map-objects/ladder'
    'scripts/tasks'
  ],
  (Map, log, MetalBlock, Player, Gun, Drone, Ladder, Tasks) ->
    log('Module: Game')

    ACTION = {
      BUILD: 'BUILD'
      REMOVE: 'REMOVE'
    }

    currentAction = null
    currentContext = null
    updateListeners = []
    map = Map.load()
    if !map
      map = Map.generate()

    doAction = (x, y) ->
      if currentAction and currentContext
        switch currentAction
          when ACTION.BUILD
            point = {
              x: x
              y: y
              z: 1
            }
            buildable = _.contains(currentContext.tags, 'buildable')
            if currentContext.canBePlaced(map, point)
              object = currentContext({
                building: buildable
                x: point.x
                y: point.y
                z: point.z
              })
              map.add(object)
              Tasks.add({
                context: object
              })
            else if currentContext.canBeBackground
              point.z = 0
              if currentContext.canBePlaced(map, point)
                object = currentContext({
                  isBackground: true
                  building: buildable
                  x: point.x
                  y: point.y
                  z: point.z
                })
                map.add(object)
                Tasks.add({
                  context: object
                })
              else
                log('Error: Tile can\'t be placed.')

            Map.save(map)
          when ACTION.REMOVE
            point = {
              x: x
              y: y
              z: 1
            }
            tile = map.get(point)
            if tile
              map.remove(point)
            else
              point.z = 0
              tile = map.get(point)
              if tile
                map.remove(point)

            Map.save(map)

    getToolbox = () ->
      return [
        {
          action: ACTION.REMOVE
          context: {
            text: 'Remove'
          }
        }
        {
          action: ACTION.BUILD
          context: MetalBlock
        }
        {
          action: ACTION.BUILD
          context: Player
        }
        {
          action: ACTION.BUILD
          context: Ladder
        }
        # {
        #   action: ACTION.BUILD
        #   context: Gun
        # }
        # {
        #   action: ACTION.BUILD
        #   context: Drone
        # }
      ]

    setAction = (action, context) ->
      currentAction = action
      currentContext = context

    step = () ->
      for key, tile of map._tiles
        if tile
          tile.update(map)
      updateListeners.forEach(
        (listener) ->
          if typeof listener == 'function'
            listener()
      )
      # Map.save(map)

    onUpdate = (listener) ->
      updateListeners.push(listener)

    animate = () ->
      step()
      setTimeout(animate, 1000)

    setTimeout(animate, 1000)
    # # TEMP
    # doStep = () ->
    #   step()

    return {
      getMap: () ->
        map
      doAction: doAction
      getToolbox: getToolbox
      setAction: setAction
      ACTION: ACTION
      onUpdate: onUpdate
      # # temp
      # doStep: doStep
    }
)