import Promise from 'promise';

const loadImage = (path) => {
  let promise = new Promise(
    (resolve, reject) => {
      let img = new Image;
      img.onload = (event) => {
        resolve(event.target);
      };
      img.src = path;
    }
  );

  return promise;
}

module.exports = {
  loadImages: function (imagesArray, cb) {
    let imgsPromises = imagesArray.map(
      (image) => {
        return loadImage(image);
      }
    );

    return Promise.all(imgsPromises);
  }
};