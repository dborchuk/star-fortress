// var log = require(['./log.js']);
import log from './log.js';

log('Module: Store');
module.exports = {
  get: function(key) {
    return JSON.parse(localStorage.getItem(key));
  },
  set: function(key, value) {
    return localStorage.setItem(key, JSON.stringify(value));
  }
};
