import _ from 'lodash';

import Map from './map.js';
import log from './log.js';
import Renderer from './renderer.js';
import rendererTools from './rendererTools.js';

import MetalBlock from './map-objects/metal-block.js';
import Frame from './map-objects/frame.js';
import Player from './map-objects/player.js';
import Ladder from './map-objects/ladder.js';

import Toolbox from './toolbox/toolbox.js';
import TaskManager from './tasks.js';
import ACTIONS from './actions.js';

let bgImg = require('../images/stars-bg.png');
class Game {
  constructor(config) {
    config = config || {};
    let w = 500;
    let h = 500;

    this.renderer = new Renderer({
      w: w,
      h: h
    });
    this.map = Map.load();
    this.taskManager = new TaskManager();
    this.toolbox = new Toolbox({
      onItemClick: (tool) => this.setAction(tool.action, tool.context)
    });
    this.toolbox.fill( getToolbox() );
    this.stage = this.renderer.getStage();
    let mapSprite = addMapContainer(this.stage, w, h, (event) => {
      const point = getPoint(event.data.global.x, event.data.global.y, 50);
      doAction({
        point: point,
        action: this.currentAction,
        context: this.currentContext,
        map: this.map,
        taskManager: this.taskManager,
        container: mapSprite
      });
    });
    let sprite = this.toolbox.getSprite();
    sprite.position.y = h - 60;
    this.stage.addChild(sprite);
    addItemsToContainer(this.map.items, mapSprite);
  }
  setAction(action, context) {
    this.currentAction = action;
    this.currentContext = context;
  };
}

function addItemToContainer(item, point, container) {
  let sprite = item.getSprite();
  sprite.position.x = point.x * 50;
  sprite.position.y = point.y * 50;
  container.addChild( sprite );
}

function addItemsToContainer(map, container) {
  for (let key in map) {
    if ( map.hasOwnProperty(key) ) {
      map[key].forEach((item) => {
        addItemToContainer(item, getPointFromKey(key), container);
      });
    }
  }
}

function getPointFromKey(key) {
  return {
    x: key.split(',')[0],
    y: key.split(',')[1]
  };
}

function addMapContainer(stage, w, h, clickCb) {
  let mapSprite = rendererTools.createSprite( rendererTools.cteateTexture(bgImg) );
  mapSprite.interactive = true;
  // mapSprite.width = w;
  // mapSprite.height = h;
  mapSprite.on('click', clickCb);
  stage.addChild(mapSprite);
  return mapSprite;
}

function getPoint(x, y, scale) {
  return {
    x: Math.floor(x / scale),
    y: Math.floor(y / scale)
  };
}

// const getResources = () => {

// };

// const setRenderer = (newRenderer) => {
//   renderer = newRenderer;
// };

// var animate, currentAction, currentContext, doAction, getToolbox, map, onUpdate, setAction, step, updateListeners;
// log('Module: Game');
// let currentAction = null;
// let currentContext = null;
// updateListeners = [];
// map = Map.load();
// if (!map) {
//   map = Map.generate();
// }
const doAction = function(config) {
  config = config || {};

  let x = config.point.x;
  let y = config.point.y;
  let action = config.action;
  let context = config.context;
  let map = config.map;
  let taskManager = config.taskManager;
  let container = config.container;

  var buildable, object, point, tile;

  let actions = {};
  actions[ACTIONS.BUILD] = () => {
    addObject(x, y, context, map, taskManager, container);
  }
  actions[ACTIONS.REMOVE] = () => {
    remove(x, y, map)
  };
  actions[action] && actions[action]();
  map.save();
};

const remove = (x, y, map) => {
  let point = {
    x: x,
    y: y
  };
  // tile = map.get(point);
  // if (tile) {
  if ( !map.remove(point) ) {
    // tile = map.get(point);
    // if (tile) {
    map.remove(point);
    // }
  }
  // return Map.save(map);
};

const addObject = (x, y, context, map, taskManager, container) => {
  let point = {
    x: x,
    y: y
  };

  // let buildable = _.includes(context.tags, 'buildable');
  if ( context.canBePlaced(map, point) ) {
    let item = new context();
  //   let object = new context({
  //     building: buildable,
  //     x: point.x,
  //     y: point.y
  //   });
    map.add(item, point);
    let sprite = item.getSprite();
    sprite.position.x = x * 50;
    sprite.position.y = y * 50;
    container.addChild( sprite );
  //   taskManager.add({
  //     context: object
  //   });
  }
  // } else if (context.canBeBackground) {
  //   if (context.canBePlaced(map, point)) {
  //     object = new context({
  //       isBackground: true,
  //       building: buildable,
  //       x: point.x,
  //       y: point.y
  //     });
  //     map.add(object);
  //     container.addChild( object.getSprite() );
  //     taskManager.add({
  //       context: object
  //     });
  //   } else {
  //     log('Error: Tile can\'t be placed.');
  //   }
  // }
};

const getToolbox = function() {
  return [
    // {
    //   action: ACTIONS.REMOVE,
    //   context: {
    //     text: 'Remove'
    //   }
    // },
    {
      action: ACTIONS.BUILD,
      context: Frame
    },
    // {
    //   action: ACTIONS.BUILD,
    //   context: MetalBlock
    // // }, {
    // //   action: ACTIONS.BUILD,
    // //   context: Player
    // },
    // {
    //   action: ACTIONS.BUILD,
    //   context: Ladder
    // }
  ];
};
// step = function() {
//   var key, ref, tile;
//   ref = map._tiles;
//   for (key in ref) {
//     tile = ref[key];
//     if (tile) {
//       tile.update(map);
//     }
//   }
//   return updateListeners.forEach(function(listener) {
//     if (typeof listener === 'function') {
//       return listener();
//     }
//   });
// };
// onUpdate = function(listener) {
//   return updateListeners.push(listener);
// };
// animate = function() {
//   step();
//   return setTimeout(animate, 1000);
// };
// setTimeout(animate, 1000);

module.exports = Game;