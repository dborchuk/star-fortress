import Promise from 'promise';
import resourceLoader from './resource-loader.js';

// const loadImage = (path) => {
//   let promise = new Promise(
//     (resolve, reject) => {
//       let img = new Image;
//       img.onload = (data) => {
//         console.log(data);
//         resolve();
//       };
//       img.src = path;
//     }
//   );

//   return promise;
// }

let resources = [];
let keys = [];

let resourcesMap = {};

let fillResourseMap = (map, keys, images) => {
  images.forEach(
    (img, index) => {
      const key = keys[index];

      map[key] = img;
    }
  );

  return map;
};

module.exports = {
  setResource: function (key, path) {
    keys.push(key);
    resources.push(path);
  },
  loadResources: () => {
    return new Promise(
      (resolve, reject) => {
        return resourceLoader.loadImages(resources).then(
          (images) => {
            return fillResourseMap(resourcesMap, keys, resources);
          }
        );
      }
    );
  },
  getResource: (key) => {
    return resourcesMap[key];
  }
};