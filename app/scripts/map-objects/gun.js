(function() {
  var log = require(['./../log.js']);
  var map = require(['./../map.js']);

  var constructor, texture, type;
  log('Module: Gun');
  type = 'Gun';
  texture = 'images/gun.png';
  constructor = function(options) {
    options = options || {};
    return {
      options: options,
      type: type,
      texture: texture
    };
  };
  constructor.canBePlaced = function(map, point) {
    var result, tile;
    result = false;
    tile = map.get(point.x, point.y, point.z - 1);
    if (tile && point.z === 1) {
      tile = map.get(point.x, point.y, point.z);
      if (!tile) {
        result = true;
      }
    }
    return result;
  };
  constructor._texture = texture;
  constructor.type = type;
  return constructor;

}).call(this);
