(function() {
  define(['scripts/log'], function(log) {
    log('Module: Soil');
    return function(options) {
      options = options || {};
      return {
        options: options
      };
    };
  });

}).call(this);
