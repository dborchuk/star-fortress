  // var log = require(['./../log.js']);
import log from './../log.js';
// var GameObject = require(['./game-object.js']);
import GameObject from './game-object.js';
var Tasks = require(['./../tasks.js']);

var build, cloneMatrix, constructor, drawMatrix, getDestinationPoints, move, texture, type;
log('Module: Player');
type = 'Player';
texture = 'images/player.png';
constructor = function(options) {
  var instance, newProps;
  instance = GameObject(options);
  newProps = {
    type: type,
    texture: texture,
    update: constructor.update,
    inheritanceArray: _.union(GameObject.inheritanceArray, [type])
  };
  _.extend(instance, newProps);
  return instance;
};
drawMatrix = function(matrix) {
  var i, j, results, row, rows;
  console.log(matrix);
  rows = [];
  i = 0;
  while (i < matrix.length) {
    row = matrix[i];
    j = 0;
    while (j < row.length) {
      if (!rows[j]) {
        rows[j] = '';
      }
      rows[j] += row[j];
      j++;
    }
    i++;
  }
  i = 0;
  results = [];
  while (i < rows.length) {
    console.log(rows[i]);
    results.push(i++);
  }
  return results;
};
cloneMatrix = function(matrix) {
  var i, newMatrix;
  newMatrix = [];
  i = 0;
  while (i < matrix.length) {
    newMatrix.push(_.clone(matrix[i]));
    i++;
  }
  return newMatrix;
};
getDestinationPoints = function(matrix, task, offsetX, offsetY) {
  var getValue, tile, walkablePoints, x, y;
  getValue = function(matrix, x, y) {
    var value;
    value = 1;
    if ((matrix != null) && (matrix[x] != null) && (matrix[x][y] != null)) {
      value = matrix[x][y];
    }
    return value;
  };
  tile = task.context;
  walkablePoints = [];
  x = tile.x - offsetX;
  y = tile.y - offsetY;
  (getValue(matrix, x, y - 1) === 0) && walkablePoints.push({
    x: x,
    y: y - 1,
    buildingTile: tile,
    task: task
  });
  (getValue(matrix, x + 1, y - 1) === 0) && walkablePoints.push({
    x: x + 1,
    y: y - 1,
    buildingTile: tile,
    task: task
  });
  (getValue(matrix, x + 1, y) === 0) && walkablePoints.push({
    x: x + 1,
    y: y,
    buildingTile: tile,
    task: task
  });
  (getValue(matrix, x + 1, y + 1) === 0) && walkablePoints.push({
    x: x + 1,
    y: y + 1,
    buildingTile: tile,
    task: task
  });
  (getValue(matrix, x, y + 1) === 0) && walkablePoints.push({
    x: x,
    y: y + 1,
    buildingTile: tile,
    task: task
  });
  (getValue(matrix, x - 1, y + 1) === 0) && walkablePoints.push({
    x: x - 1,
    y: y + 1,
    buildingTile: tile,
    task: task
  });
  (getValue(matrix, x - 1, y) === 0) && walkablePoints.push({
    x: x - 1,
    y: y,
    buildingTile: tile,
    task: task
  });
  (getValue(matrix, x - 1, y - 1) === 0) && walkablePoints.push({
    x: x - 1,
    y: y - 1,
    buildingTile: tile,
    task: task
  });
  return walkablePoints;
};
constructor.update = function(map) {
  var _this, destinationPoints, keys, matrix, minPath, minPathResult, moved, nextStep, paths, source, subscription, tasks;
  GameObject.update.call(this);
  _this = this;
  keys = Object.keys(map._tiles);
  source = Rx.Observable.fromArray(keys).map(function(key) {
    return map._tiles[key];
  }).filter(function(tile) {
    return !!tile;
  });
  if (this.task) {
    tasks = [this.task];
  } else {
    tasks = Tasks.getAll();
  }
  if (tasks.length > 0) {
    matrix = map.getPassMatrix(map, _this.y, _this.z);
    destinationPoints = [];
    _.each(tasks, function(task) {
      return destinationPoints = _.union(destinationPoints, getDestinationPoints(matrix, task, matrix.offsetX, matrix.offsetY));
    });
    paths = Rx.Observable.fromArray(destinationPoints).map(function(point) {
      var error, error1, finder, grid, matrixPath, result, tempMatrix;
      result = false;
      tempMatrix = cloneMatrix(matrix);
      finder = new PF.AStarFinder();
      grid = new PF.Grid(tempMatrix);
      try {
        matrixPath = finder.findPath(_this.y - matrix.offsetY, _this.x - matrix.offsetX, point.y, point.x, grid);
        result = matrixPath.map(function(point) {
          return {
            x: point[1] + matrix.offsetX,
            y: point[0] + matrix.offsetY
          };
        });
      } catch (error1) {
        error = error1;
      }
      result.buildingTile = point.buildingTile;
      result.task = point.task;
      return result;
    }).filter(function(path) {
      return path.length > 0;
    });
    minPath = paths.minBy(function(path) {
      return path.length;
    });
    minPathResult = null;
    subscription = minPath.subscribe(function(paths) {
      return minPathResult = paths[0];
    });
    if (minPathResult) {
      this.task = minPathResult.task;
      this.task.assignee = this;
      source = Rx.Observable.from(minPathResult);
      nextStep = source.skip(1).take(1);
      moved = false;
      subscription = nextStep.subscribe(function(step) {
        move(map, _this, step.x, step.y, _this.z);
        return moved = true;
      });
      if (!moved) {
        build(map, minPathResult.buildingTile.x, minPathResult.buildingTile.y);
        this.task.done();
        return this.task = null;
      }
    } else {
      return this.task = null;
    }
  }
};
build = function(map, x, y) {
  var tile;
  tile = map.get(x, y, 0);
  if (tile.building) {
    return tile.building = false;
  } else {
    tile = map.get(x, y, 1);
    if (tile.building) {
      return tile.building = false;
    }
  }
};
move = function(map, instance, x, y, z) {
  instance.x = x;
  instance.y = y;
  return instance.z = z;
};
constructor.canBePlaced = function(map, point) {
  var result, tile;
  result = false;
  tile = map.get(point.x, point.y + 1, point.z);
  if (tile) {
    tile = map.get(point.x, point.y, point.z);
    if (!tile) {
      result = true;
    }
  }
  return result;
};
constructor._texture = texture;
constructor.type = type;

module.exports = constructor;
