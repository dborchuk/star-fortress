define(
  [
    'scripts/log'
    'scripts/map'
    'scripts/map-objects/game-object'
  ],
  (log, Map, GameObject) ->
    log('Module: MetalBlock')
    type = 'MetalBlock'
    texture = 'images/metal-block.png'
    textureBg = 'images/metal-block-bg.png'

    constructor = (options) ->
      instance = GameObject(options)

      isBackground = options.isBackground

      newProps = {
        type: type
        texture: if isBackground then textureBg else texture
        tags: constructor.tags
        inheritanceArray: _.union(GameObject.inheritanceArray, [type])
      }
      _.extend(instance, newProps)

      return instance

    constructor.canBePlaced = (map, point) ->
      result = false
      tile = map.get(point.x, point.y, point.z)
      if point.z == 0
        if !tile
          result = true
      else if !tile
        tile = map.get(point.x, point.y, point.z - 1)
        if tile
          result = true
      return result

    constructor._texture = texture
    constructor.type = type
    constructor.tags = _.union(GameObject.tags, ['buildable', 'blocker', 'support'])

    constructor.canBeBackground = true

    return constructor
)