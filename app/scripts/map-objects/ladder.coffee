define(
  [
    'scripts/log'
    'scripts/map-objects/game-object'
  ],
  (log, GameObject) ->
    type = 'Ladder'
    texture = 'images/ladder.png'

    constructor = (options) ->
      instance = GameObject(options)

      newProps = {
        type: type
        texture: texture
        tags: constructor.tags
        inheritanceArray: _.union(GameObject.inheritanceArray, [type])
      }
      _.extend(instance, newProps)

      return instance

    constructor.canBePlaced = (map, point) ->
      return !!map.get(point.x, point.y, point.z - 1)

    constructor._texture = texture
    constructor.type = type
    constructor.tags = _.union(GameObject.tags, ['buildable', 'support'])

    return constructor
)