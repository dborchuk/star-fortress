define(
  [
    'scripts/log'
    'scripts/map'
    'scripts/map-objects/game-object'
  ],
  (log, Map, GameObject) ->
    log('Module: Drone')
    type = 'Drone'
    texture = 'images/drone.png'

    defaults = {
      range: 3
      damage: 5
    }

    update = () ->
      if (@x and @y and @z)
        range = @options.range or defaults.range
        map = Map.load()
        coordsArray = []
        x = @x - range
        while x <= @x + range
          y = @y - range
          while y <= @y + range
            coordsArray.push({x: x, y: y})
            y++
          x++

        source = rx.Observable.fromArray(coordsArray)
          .map((coord) -> map.get(coord.x, coord.y, @z))
          .filter((tile) -> !!tile)
          .first((tile) -> tile.damage(@options.damage or defaults.damage))

    constructor = (options) ->
      options = options or {}
      instance = GameObject(options)
      
      newProps = {
        type: type
        texture: texture
        update: update
      }
      _.extend(instance, newProps)

      return instance

    constructor.canBePlaced = (map, point) ->
      result = false
      if GameObject.canBePlaced()
        tile = map.get(point.x, point.y, point.z)
        if !tile
          result = true
      return result

    constructor._texture = texture
    constructor.type = type

    return constructor
)