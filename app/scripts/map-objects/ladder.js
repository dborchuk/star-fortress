(function() {
  var log = require(['./../log.js']);
  var GameObject = require(['./game-object.js']);

  var constructor, texture, type;
  type = 'Ladder';
  texture = 'images/ladder.png';
  constructor = function(options) {
    var instance, newProps;
    instance = new GameObject(options);
    newProps = {
      type: type,
      texture: texture,
      tags: constructor.tags,
      inheritanceArray: _.union(GameObject.inheritanceArray, [type])
    };
    _.extend(instance, newProps);
    return instance;
  };
  constructor.canBePlaced = function(map, point) {
    return !!map.get(point.x, point.y, point.z - 1);
  };
  constructor._texture = texture;
  constructor.type = type;
  constructor.tags = _.union(GameObject.tags, ['buildable', 'support']);
  return constructor;

}).call(this);
