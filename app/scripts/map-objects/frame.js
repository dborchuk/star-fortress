import _ from 'lodash';
import log from './../log.js';
// import Map from './../map.js';
// import resourceManager from './../resource-manager.js';
import GameObject from './game-object.js';
import rendererTools from './../rendererTools.js';

let img = require('../../images/frame.png');

log('Module: Frame');

// resourceManager.setResource(img);

class Frame extends GameObject {
  constructor(options) {
    super(options);

    // this.type = this.getType();
    this.tags = _.union(this.tags, ['buildable', 'blocker', 'support']);
  }
  getSprite() {
    let sprite = rendererTools.createSprite( rendererTools.cteateTexture(img) );
    sprite.height = this.h;
    sprite.width = this.w;
    // sprite.position.x = this.x;
    // sprite.position.y = this.y;
    return sprite;
  }
  static canBePlaced(map, point) {
    var result = false,
      tile = map.get(point);

    return !tile.find( item => item.type === this.getType());
  }
  static getType() {
    return 'Frame';
  }
}

module.exports = Frame;