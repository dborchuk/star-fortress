define(
  [
    'scripts/log'
    'scripts/map'
  ],
  (log, Map) ->
    log('Module: Gun')
    type = 'Gun'
    texture = 'images/gun.png'

    constructor = (options) ->
      options = options or {}

      return {
        options: options
        type: type
        texture: texture
      }

    constructor.canBePlaced = (map, point) ->
      result = false
      tile = map.get(point.x, point.y, point.z - 1)
      if tile and point.z == 1
        tile = map.get(point.x, point.y, point.z)
        if !tile
          result = true
      return result

    constructor._texture = texture
    constructor.type = type

    return constructor
)