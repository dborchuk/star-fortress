define(
  [
    'scripts/log'
    'scripts/map-objects/game-object'
    'scripts/tasks'
  ],
  (log, GameObject, Tasks) ->
    log('Module: Player')
    type = 'Player'
    texture = 'images/player.png'

    constructor = (options) ->
      instance = GameObject(options)

      newProps = {
        type: type
        texture: texture
        update: constructor.update
        inheritanceArray: _.union(GameObject.inheritanceArray, [type])
      }
      _.extend(instance, newProps)

      return instance

    drawMatrix = (matrix) ->
      console.log(matrix)
      rows = []
      i = 0
      while i < matrix.length
        row = matrix[i]
        j = 0
        while j < row.length
          if !rows[j]
            rows[j] = ''
          rows[j] += row[j]
          j++
        i++

      i = 0
      while i < rows.length
        console.log(rows[i])
        i++

    cloneMatrix = (matrix) ->
      newMatrix = []
      i = 0
      while i < matrix.length
        newMatrix.push( _.clone(matrix[i]) )
        i++
      return newMatrix

    getDestinationPoints = (matrix, task, offsetX, offsetY) ->
      getValue = (matrix, x, y) ->
        value = 1
        if matrix? and matrix[x]? and matrix[x][y]?
          value = matrix[x][y]
        return value

      tile = task.context

      walkablePoints = []
      x = tile.x - offsetX
      y = tile.y - offsetY
      # top
      (getValue(matrix, x, y - 1) == 0) and walkablePoints.push({x: x, y: y - 1, buildingTile: tile, task: task})
      # top right
      (getValue(matrix, x + 1, y - 1) == 0) and walkablePoints.push({x: x + 1, y: y - 1, buildingTile: tile, task: task})
      # right
      (getValue(matrix, x + 1, y) == 0) and walkablePoints.push({x: x + 1, y: y, buildingTile: tile, task: task})
      # bottom right
      (getValue(matrix, x + 1, y + 1) == 0) and walkablePoints.push({x: x + 1, y: y + 1, buildingTile: tile, task: task})
      # bottom
      (getValue(matrix, x, y + 1) == 0) and walkablePoints.push({x: x, y: y + 1, buildingTile: tile, task: task})
      # bottom left
      (getValue(matrix, x - 1, y + 1) == 0) and walkablePoints.push({x: x - 1, y: y + 1, buildingTile: tile, task: task})
      # left
      (getValue(matrix, x - 1, y) == 0) and walkablePoints.push({x: x - 1, y: y, buildingTile: tile, task: task})
      # top left
      (getValue(matrix, x - 1, y - 1) == 0) and walkablePoints.push({x: x - 1, y: y - 1, buildingTile: tile, task: task})

      return walkablePoints


    constructor.update = (map) ->
      GameObject.update.call(@)
      _this = @

      # ------------------------------------------------------
      # Find tasks
      keys = Object.keys(map._tiles)
      source = Rx.Observable.fromArray(keys)
        .map( (key) -> map._tiles[key] )
        .filter( (tile) -> !!tile )

      # buildingTiles = source.filter( (tile) -> tile.building )
      # count = 0
      # buildingTiles.count().subscribe( (buildingsCount) -> count = buildingsCount )
      # ------------------------------------------------------

      if @task
        tasks = [@task]
      else
        tasks = Tasks.getAll()

      if tasks.length > 0
        matrix = map.getPassMatrix(map, _this.y, _this.z);
        # buildingTiles = source.filter( (tile) -> tile.building )
        #   .filter( (tile) -> tile.x >= matrix.offsetX )
        #   .filter( (tile) -> tile.x <= matrix.lengthX + matrix.offsetX - 1 )
        #   .filter( (tile) -> tile.y >= matrix.offsetY )
        #   .filter( (tile) -> tile.y <= matrix.lengthY + matrix.offsetY - 1 )

        destinationPoints = []
        # subscription = buildingTiles.subscribe(
        #   (tile) ->
        #     destinationPoints = _.union(
        #       destinationPoints,
        #       getDestinationPoints(matrix, tile, matrix.offsetX, matrix.offsetY)
        #     )
        # )
        _.each(
          tasks,
          (task) ->
            destinationPoints = _.union(
              destinationPoints,
              getDestinationPoints(matrix, task, matrix.offsetX, matrix.offsetY)
            )
        )
        # drawMatrix(matrix)
        paths = Rx.Observable.fromArray(destinationPoints)
          .map(
            (point) ->
              result = false
              tempMatrix = cloneMatrix(matrix)
              # tempMatrix[point.x - offsetX][point.y - offsetY] = 0
              # drawMatrix(tempMatrix)
              finder = new PF.AStarFinder()
              grid = new PF.Grid(tempMatrix)
              try
                matrixPath = finder.findPath(
                  _this.y - matrix.offsetY,
                  _this.x - matrix.offsetX,
                  point.y,
                  point.x,
                  grid
                )
                result = matrixPath.map( (point) -> {x: point[1] + matrix.offsetX, y: point[0] + matrix.offsetY} )
              catch error

              result.buildingTile = point.buildingTile
              result.task = point.task

              return result
          )
          .filter( (path) -> path.length > 0 )

        minPath = paths.minBy( (path) -> path.length )

        minPathResult = null
        subscription = minPath.subscribe( (paths) -> minPathResult = paths[0] )

        if minPathResult
          @task = minPathResult.task
          @task.assignee = @

          source = Rx.Observable.from(minPathResult)
          nextStep = source.skip(1)
            .take(1)

          moved = false
          subscription = nextStep.subscribe(
            (step) ->
              move(map, _this, step.x, step.y, _this.z)
              moved = true
          )

          if !moved
            build(map, minPathResult.buildingTile.x, minPathResult.buildingTile.y)
            @task.done()
            @task = null
            # source.takeLast(1)
            #   .subscribe( (point) ->  )
        else
          @task = null

    build = (map, x, y) ->
      tile = map.get(x, y, 0)
      if tile.building
        tile.building = false
      else
        tile = map.get(x, y, 1)
        if tile.building
          tile.building = false

    move = (map, instance, x, y, z) ->
      # tile = map.get(x, y, z)
      # if !tile
      #   map.set(instance.x, instance.y, instance.z, null)
      #   map.set(x, y, z, instance)
      instance.x = x
      instance.y = y
      instance.z = z

    constructor.canBePlaced = (map, point) ->
      result = false
      tile = map.get(point.x, point.y + 1, point.z)
      if tile
        tile = map.get(point.x, point.y, point.z)
        if !tile
          result = true
      return result

    constructor._texture = texture
    constructor.type = type

    return constructor
)