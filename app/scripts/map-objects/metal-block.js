// var log = require(['./../log.js']);
import log from './../log.js';
// var Map = require(['./../map.js']);
import Map from './../map.js';
// var GameObject = require(['./game-object.js']);
import GameObject from './game-object.js';
import _ from 'lodash';

// var constructor, texture, textureBg, type;
// log('Module: MetalBlock');
// type = 'MetalBlock';
// texture = 'images/metal-block.png';
// textureBg = 'images/metal-block-bg.png';
// constructor = function(options) {
//   var instance, isBackground, newProps;
//   instance = new GameObject(options);
//   isBackground = options.isBackground;
//   newProps = {
//     type: type,
//     texture: isBackground ? textureBg : texture,
//     tags: constructor.tags,
//     inheritanceArray: _.union(GameObject.inheritanceArray, [type])
//   };
//   _.extend(instance, newProps);
//   return instance;
// };
// constructor.canBePlaced = function(map, point) {
//   var result, tile;
//   result = false;
//   tile = map.get(point.x, point.y, point.z);
//   if (point.z === 0) {
//     if (!tile) {
//       result = true;
//     }
//   } else if (!tile) {
//     tile = map.get(point.x, point.y, point.z - 1);
//     if (tile) {
//       result = true;
//     }
//   }
//   return result;
// };
// constructor._texture = texture;
// constructor.type = type;
// constructor.tags = _.union(GameObject.tags, ['buildable', 'blocker', 'support']);
// constructor.canBeBackground = true;

class MetalBlock extends GameObject {
  constructor(options) {
    super(options);

    this.type = 'MetalBlock';
    this.tags = _.union(this.tags, ['buildable', 'blocker', 'support']);
  }
  canBePlaced(map, point) {
    var result, tile;
    result = false;
    tile = map.get(point.x, point.y, point.z);
    if (point.z === 0) {
      if (!tile) {
        result = true;
      }
    } else if (!tile) {
      tile = map.get(point.x, point.y, point.z - 1);
      if (tile) {
        result = true;
      }
    }
    return result;
  }
}

module.exports = MetalBlock;