import log from './../log.js';
import rendererTools from './../rendererTools.js';

class GameObject {
  constructor(options) {
    this.type = this.constructor.getType();
    this.tags = [];
    this.w = 50;
    this.h = 50;
  }
  canBePlaced(map, point) {
    return true;
  }
  getSprite() {
    return rendererTools.createSprite( rendererTools.getEmptyTexture() );
  }
  update() {
    return true;
  }
  static canBePlaced() {
    return true;
  }
  static getType() {
    return 'GameObject';
  }
}

module.exports = GameObject;