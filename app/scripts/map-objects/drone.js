(function() {
  var log = require(['./../log.js']);
  var map = require(['./../map.js']);
  var GameObject = require(['./game-object.js']);

  var constructor, defaults, texture, type, update;
  log('Module: Drone');
  type = 'Drone';
  texture = 'images/drone.png';
  defaults = {
    range: 3,
    damage: 5
  };
  update = function() {
    var coordsArray, map, range, source, x, y;
    if (this.x && this.y && this.z) {
      range = this.options.range || defaults.range;
      map = Map.load();
      coordsArray = [];
      x = this.x - range;
      while (x <= this.x + range) {
        y = this.y - range;
        while (y <= this.y + range) {
          coordsArray.push({
            x: x,
            y: y
          });
          y++;
        }
        x++;
      }
      return source = rx.Observable.fromArray(coordsArray).map(function(coord) {
        return map.get(coord.x, coord.y, this.z);
      }).filter(function(tile) {
        return !!tile;
      }).first(function(tile) {
        return tile.damage(this.options.damage || defaults.damage);
      });
    }
  };
  constructor = function(options) {
    var instance, newProps;
    options = options || {};
    instance = GameObject(options);
    newProps = {
      type: type,
      texture: texture,
      update: update
    };
    _.extend(instance, newProps);
    return instance;
  };
  constructor.canBePlaced = function(map, point) {
    var result, tile;
    result = false;
    if (GameObject.canBePlaced()) {
      tile = map.get(point.x, point.y, point.z);
      if (!tile) {
        result = true;
      }
    }
    return result;
  };
  constructor._texture = texture;
  constructor.type = type;
  return constructor;

}).call(this);
