import Rx from 'rxjs/Rx';

const NO_LAYER = 'NO_LAYER';

const clickHandler = () => {
  console.log('click');
};

const keyupHandler = () => {
  console.log('keyup');
};

const splitByLayers = (map) => {
  let layers = {};

  map.forEach(
    (item) => {
      layers[(item.layer || NO_LAYER)] = item;
    }
  );

  return layers;
};

const drawLayer = (layer) => {
  layer.forEach(drawItem);
};

const drawItem = (item) => {
  console.log('drawItem')
}

class Drawer {
  constructor(config) {
    config = config || {};

    let container = config.container || document.body;

    this.layers = config.layers || [];
    this.size = config.size || {
      w: 600,
      h: 600
    };

    this.canvas = document.createElement('canvas');
    this.canvas.addEventListener('click', clickHandler);
    this.canvas.addEventListener('keyup', keyupHandler);
    this.canvas.width = this.size.w;
    this.canvas.height = this.size.h;
    container.appendChild(this.canvas);
  }
  draw(map) {
    const mapLayers = splitByLayers(map);

    this.layers.forEach(
      (layer) => {
        drawLayer(mapLayers[layer]);
      }
    );
    drawLayer(mapLayers[NO_LAYER] || []);
  }
  setLayers(layers) {
    this.layers = layers;
  }
}

module.exports = Drawer;

(function init() {
  window.Drawer = {};

  Drawer.init = function (options) {
    options = options || {};

    this.canvas = options.canvas;
    this.context = this.canvas.getContext('2d');
    this.tileSize = options.tileSize || 40;

    this.canvas.addEventListener('click', clickHandler);
  };

  function clickHandler(e) {
    log(e);
  }

  function drawTile(tile, context, x, y) {
    if (tile) {
      tile.forEach(function tileForEach(item) {
        drawItem(item, context, x, y);
      });
    }
  }

  function drawItem(item, context, x, y) {
    // context.drawImage(item.getCanvas(), x * Drawer.tileSize, y * Drawer.tileSize);
    var itemCanvas = item.getCanvas();
    context.drawImage(
      itemCanvas,
      0,
      0,
      itemCanvas.width,
      itemCanvas.height,
      x * Drawer.tileSize,
      y * Drawer.tileSize,
      Drawer.tileSize,
      Drawer.tileSize);
  }

  function drawLayer(layer, context) {
    for (var i = 0; i < layer.length; i++) {
      if (layer[i]) {
        for (var j = 0; j < layer[i].length; j++) {
          drawTile(layer[i][j], context, i, j);
        }
      }
    }
    // layer.forEach(function layerForEach(tile) {
    //   drawTile(tile, context);
    // });
  }

  Drawer.draw = function (map) {
    drawLayer(map.back, this.context);
    drawLayer(map.front, this.context);
  };
});