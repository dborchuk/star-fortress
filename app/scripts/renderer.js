import PIXI from 'pixi.js';

class Renderer {
  constructor(config) {
    config = config || {};

    let renderer = PIXI.autoDetectRenderer(config.w, config.h,{backgroundColor : 0x1099bb});
    document.body.appendChild(renderer.view);

    // create the root of the scene graph
    this.stage = new PIXI.Container();
    startAnimation(renderer, this.stage);
  }
  getStage() {
    return this.stage;
  }
}

function startAnimation(renderer, stage) {
  // start animating
  animate();
  function animate() {
    requestAnimationFrame(animate);
    // render the container
    renderer.render(stage);
  }
}


// // create a texture from an image path
// var texture = PIXI.Texture.fromImage('required/assets/basics/bunny.png');

// // create a new Sprite using the texture
// var bunny = new PIXI.Sprite(texture);

// // center the sprite's anchor point
// bunny.anchor.x = 0.5;
// bunny.anchor.y = 0.5;

// // move the sprite to the center of the screen
// bunny.position.x = 200;
// bunny.position.y = 150;

// stage.addChild(bunny);


module.exports = Renderer;