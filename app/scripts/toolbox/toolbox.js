import rendererTools from './../rendererTools.js';
import ToolboxItem from './toolboxItem.js';

let bgImg = require('../../images/toolbar.png');

class Toolbox {
  constructor(config) {
    config = config || {};

    this.container = rendererTools.createSprite( rendererTools.cteateTexture(bgImg) );
    // this.container.height = 50;
    // this.container.width = 50;
    this.onItemClick = config.onItemClick;
  }
  fill(items) {
    fillToolbox(this.container, items || [], this.onItemClick);
  }
  getSprite() {
    return this.container;
  }
}

function fillToolbox(toolbox, items, onItemClick) {
  let padding = 5;

  rendererTools.clearContainer(toolbox);
  items.forEach(
    (item, index) => {
      let itemEl;

      let sprite = getSprite(item, onItemClick);
      sprite.x = padding + (index * (50 + padding));
      sprite.y = padding;
      toolbox.addChild( sprite );
    }
  );

  return toolbox;
}

function getSprite(item, onItemClick) {
  let toolboxItem;
  let config = {
    onClick: onItemClick.bind(this, item)
  };

  if (typeof item.context === 'function') {
    let el = new item.context();
    config.element = el.getSprite();
  } else {
    config.text = item.context.text;
  }
  toolboxItem = new ToolboxItem(config);

  return toolboxItem.getSprite();
}

module.exports = Toolbox;