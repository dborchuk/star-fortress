import rendererTools from './../rendererTools.js';
import ToolboxItem from './toolboxItem.js';

class ToolboxItemImage extends ToolboxItem {
  constructor(config) {
    super(config);
    config = config || {};

    let texture = rendererTools.createTexture(config.texturePath);
    this.element = rendererTools.createSprite(texture);
  }
  getSprite() {
    return this.element;
  }
}

module.exports = ToolboxItemImage;