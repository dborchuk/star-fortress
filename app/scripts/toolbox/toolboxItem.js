import rendererTools from './../rendererTools.js';

class ToolboxItem {
  constructor(config) {
    config = config || {};

    this.sprite = rendererTools.createSprite( rendererTools.getEmptyTexture() );
    if (config.element) {
      this.sprite.addChild(config.element);
    }

    if (config.text) {
      let textSprite = rendererTools.createText(config.text);
      textSprite.width = 50;
      textSprite.height = 50;
      this.sprite.addChild(textSprite);
    }

    this.sprite.interactive = true;
    this.sprite.on('click', config.onClick);

  }
  getSprite() {
    return this.sprite;
  }
}

module.exports = ToolboxItem;